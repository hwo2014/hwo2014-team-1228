var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
/*    return send({
        msgType: "joinRace",
        data: {
            botId: {
                name: botName,
                key: botKey
            },
            trackName: "keimola",
            password: "fteamvodoi",
            carCount: 3
        }
    });
*/
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

var myCarId;
var myCar;
var pieces;
var lanes;

var throttle = 1.0;

var prevV = 0.0;
var prevPos = 0.0;
var prevAngle = 0.0;

var v0 = 1.0;
var a0 = 0.0;
var v1 = 1.0;

var switched = false;

function acceleration(v0, a0, velocity) {
    if (a0 >= 0.0) {
        return 0.0;
    }

    var v = v0;
    var a = a0;
    if (v0 < velocity) {
        while (v < velocity) {
            v = v - a;
            a /= 0.98;
        }
    } else {
        while (v > velocity) {
            a *= 0.98;
            v = v + a;
        }
    }

    return a;
}

function lane() {
    this.index = 0;
    this.lengthPath = 0.0;
}

function piece() {
    this.index = 0;
    this.lanes = [];
}

function path() {
    this.startIdx = 0;
    this.lengthPath = 0.0;
}

var switchPieces = [];
var switchPieceIndex = 0;

var distance = 0.0;

var lapTotal = 10;

var turboOn = false;

var paths = [];
var longestPathIndex = 0;

var Fslip = 0.5;
var FslipCoef = 1.0;
var FslipNoCrash = 0.9;
var FslipCrash = 1.1;
var numCrash = 0;

var qualifying = false;

function RadToDeg(rad) {
    return rad * 180.0 / Math.PI;
}

function DegToRad(deg) {
    return deg * Math.PI / 180.0;
}

function circularPiece() {
    this.start = 0;
    this.end = 0;
    this.totalAngle = 0.0;
    this.speedUp = false;
}

var circularPieces = [];

var twoLap = 0;

jsonStream.on('data', function(data) {
    if (data.msgType === 'carPositions') {
        var sent = false;
        var cars = data.data;
        var enemies = [];
        var myCarIdx = 0;
        for (var i = 0; i < cars.length; i++) {
            if (cars[i].id.name === myCarId.name && cars[i].id.color === myCarId.color) {
                myCarIdx = i;
            } else {
                enemies.push(cars[i]);
            }
        }

        var idx = data.data[myCarIdx].piecePosition.pieceIndex;
        var lap = data.data[myCarIdx].piecePosition.lap;
        var inPieceDistance = data.data[myCarIdx].piecePosition.inPieceDistance;

        var laneIdx = 0;
        for (var i = 0; i < lanes.length; i++) {
            if (lanes[i].index === data.data[myCarIdx].piecePosition.lane.endLaneIndex) {
                laneIdx = i;
            }
        }

        if (pieces[idx].switch === true) {
            switched = false;
        }

        var distToNextBend = 0.0;
        var v = inPieceDistance - prevPos;
        if (v < 0.0) {
            v = distance - prevPos + inPieceDistance;
            if (v < 0.0) {
                v = prevV;
            }
        }

        var iii = idx + 1;
        if (pieces[idx].length === undefined) {
            var radius = pieces[idx].radius;
            var angle = data.data[myCarIdx].angle;
            if (pieces[idx].angle > 0) {
                radius -= lanes[laneIdx].distanceFromCenter;
            } else {
                radius += lanes[laneIdx].distanceFromCenter;
            }

            distance = Math.PI * radius * Math.abs(pieces[idx].angle) / 180.0;

            var A = angle - prevAngle;
            if (pieces[idx].angle < 0) {
                A *= -1.0;
                angle *= -1.0;
            }
            var B = RadToDeg(v / radius);
//            console.log('B = ' + B + ' --- A = ' + A + ' --- Fslip = ' +Fslip);

            var aaa = Math.abs(pieces[idx].angle) - inPieceDistance * 180.0 / (Math.PI * radius);
            while (iii < pieces.length && pieces[iii].radius === pieces[idx].radius && pieces[iii].angle * pieces[idx].angle > 0.0) {
                aaa += Math.abs(pieces[iii].angle);
                iii++;
            }

            var maxSpeed = Math.sqrt(FslipCoef * Fslip * radius);
            if (v < maxSpeed) {
                throttle = 1.0;
            } else {
                throttle = 0.0;
            }

            if (A > 0.0 && (60.0 - angle) / A  < 2.0) {
                throttle = 0.0;
            }

            if (turboOn === true && idx + 1 === paths[longestPathIndex].startIdx) {
                if (inPieceDistance < 0.3 * distance) {
                    throttle = 0.0;
                } else if ((inPieceDistance > 0.7 * distance) && (sent === false)) {
                    send({"msgType": "turbo", "data": "Uppppppppp!"});
                    sent = true;
                    turboOn = false;
                    console.log('lap: ' + lap + ' index: ' + idx + ' inPieceDistance: ' + inPieceDistance + ' --- ' + radius + ' ' + pieces[idx].angle);
                }
            }

//            console.log('lap: ' + lap + ' index: ' + idx + ' inPieceDistance: ' + inPieceDistance + ' --- ' + radius + ' ' + pieces[idx].angle);
//            console.log('max speed: ' + maxSpeed);
        }
        else {
//            console.log('lap: ' + lap + ' index: ' + idx + ' inPieceDistance: ' + inPieceDistance +' --- ' + pieces[idx].length);
            if (turboOn === true && idx === paths[longestPathIndex].startIdx && (sent === false)) {
                send({"msgType": "turbo", "data": "Uppppppppp!"});
                sent = true;
                turboOn = false;
                console.log('lap: ' + lap + ' index: ' + idx + ' inPieceDistance: ' + inPieceDistance +' --- ' + pieces[idx].length);
            }
            throttle = 1.0;
            distance = pieces[idx].length;
        }

        if (data.data[myCarIdx].piecePosition.lane.startLaneIndex !== data.data[myCarIdx].piecePosition.lane.endLaneIndex) {
            distance *= 1.05;
        }

        distToNextBend = distance - inPieceDistance;

        while (iii < pieces.length && pieces[iii].radius === undefined) {
            distToNextBend += pieces[iii].length;
            iii++;
        }
        if (iii > pieces.length - 1 && lap < lapTotal - 1) {
            iii = 0;
            while (pieces[iii].radius === undefined) {
                distToNextBend += pieces[iii].length;
                iii++;
            }
        }

        if (iii < pieces.length) {
            var r = pieces[iii].radius;

            if (distToNextBend < 0) {
                distToNextBend = 0.1 * inPieceDistance;
            }
            var s = 0.0;
            var velo = v;
            var a = acceleration(v0, a0, v);
            var zzz = 0;
            while (zzz < 49 && s < distToNextBend && velo > 0) {
                a *= 0.98;
                velo += a;
                s += velo;
                zzz++;
            }

if (!qualifying || (qualifying && data.gameTick > 50)) {
            if (velo > Math.sqrt(FslipCoef * Fslip * r)) {
                throttle = 0.0;
            }
}
        }

        if (idx + 1 === switchPieces[switchPieceIndex].index && inPieceDistance > 0.7 * distance && !switched && !sent) {
            var nextSwitchPieceIdx = (switchPieceIndex + 1) % switchPieces.length;
            var lCount = -1;
            if (laneIdx > 0) {
                lCount = 0;
            }
            var rCount = -1;
            if (laneIdx < lanes.length - 1) {
                rCount = 0;
            }
            var mCount = 0;
            for (var i = 0; i < enemies.length; i++) {
                if (enemies[i].piecePosition.pieceIndex >= switchPieces[switchPieceIndex].index &&
                    enemies[i].piecePosition.pieceIndex < switchPieces[nextSwitchPieceIdx].index) {
                    if (lCount >= 0 && lanes[laneIdx - 1].index === enemies[i].piecePosition.lane.endLaneIndex) {
                        lCount++;
                    }
                    if (rCount >= 0 && lanes[laneIdx + 1].index === enemies[i].piecePosition.lane.endLaneIndex) {
                        rCount++;
                    }
                    if (lanes[laneIdx].index === enemies[i].piecePosition.lane.endLaneIndex) {
                        mCount++;
                    }
                }
            }
            if (lCount < 0) {
                lCount = enemies.length;
            }
            if (rCount < 0) {
                rCount = enemies.length;
            }

            if (rCount === mCount && mCount <= lCount && laneIdx < lanes.length - 1 &&
                switchPieces[switchPieceIndex].lanes[laneIdx].lengthPath > switchPieces[switchPieceIndex].lanes[laneIdx + 1].lengthPath) {
                send({"msgType": "switchLane", "data": "Right"});
                sent = true;
            } else if (lCount === mCount && mCount <= rCount && laneIdx > 0 &&
                       switchPieces[switchPieceIndex].lanes[laneIdx].lengthPath > switchPieces[switchPieceIndex].lanes[laneIdx - 1].lengthPath) {
                send({"msgType": "switchLane", "data": "Left"});
                sent = true;
            } else if (rCount < mCount && rCount < lCount) {
                send({"msgType": "switchLane", "data": "Right"});
                sent = true;
            } else if (lCount < mCount && lCount < rCount) {
                send({"msgType": "switchLane", "data": "Left"});
                sent = true;
            }

            switchPieceIndex = nextSwitchPieceIdx;
            switched = true;
        }

if (qualifying) {
        if (data.gameTick > 37 && data.gameTick < 41) {
            throttle = 0.0;
            if (data.gameTick === 39) {
                v1 = v;
            } else if (data.gameTick === 40) {
                v0 = v;
                a0 = v0 - v1;
            }
        }
}

        if (sent === false) {
            send({
                msgType: "throttle",
                data: throttle
            });
        }

        var a = v - prevV;
        prevPos = inPieceDistance;
        prevAngle = data.data[myCarIdx].angle;
        prevV = v;
//        console.log('throttle: ' + throttle + ' angle: ' + prevAngle + ' v: ' + v + ' a: ' + a + ' tick: ' + data.gameTick);
        for (var i = 0; i < enemies; i++) {
            enemies.pop();
        }
    } else {
        if (data.msgType === 'join') {
            console.log('Joined');
            throttle = 1.0;
    	} else if (data.msgType === 'yourCar') {
	        myCarId = data.data;
	        console.log('name: ' + myCarId.name + ' color: ' + myCarId.color);
    	} else if (data.msgType === 'gameInit') {
            qualifying = (data.data.race.raceSession.quickRace === undefined);
if (!qualifying) {
            lapTotal = data.data.race.raceSession.laps;
            FslipCoef = FslipNoCrash;
}

	        pieces = data.data.race.track.pieces;
            lanes = data.data.race.track.lanes;
            cars = data.data.race.cars;
            var i = 0;
            while (i < cars.length && (cars[i].id.name !== myCarId.name || cars[i].id.color !== myCarId.color)) {
                i++;
            }
            myCar = cars[i];

            for (var i = 0; i < lanes.length - 1; i++) {
                for (var j = i + 1; j < lanes.length; j++) {
                    if (lanes[i].distanceFromCenter > lanes[j].distanceFromCenter) {
                        var tmp = lanes[i];
                        lanes[i] = lanes[j];
                        lanes[j] = tmp;
                    }
                }
            }

            for (var i = 0; i < pieces.length; i++) {
                if (pieces[i].switch === true) {
                    var myPiece = new piece();
                    myPiece.index = i;
                    for (var j = 0; j < lanes.length; j++) {
                        var myLane = new lane();
                        myLane.index = lanes[j].index;
                        myPiece.lanes.push(myLane);
                    }
                    switchPieces.push(myPiece);
                }
            }

            for (var k = 0; k < switchPieces.length - 1; k++) {
                for (var j = 0; j < lanes.length; j++) {
                    for (var i = switchPieces[k].index + 1; i < switchPieces[k + 1].index; i++) {
                        var len = 0.0;
                        if (pieces[i].length !== undefined) {
                            len = pieces[i].length;
                        } else {
                            var radius = pieces[i].radius;
                            if (pieces[i].angle > 0) {
                                radius -= lanes[j].distanceFromCenter;
                            } else {
                                radius += lanes[j].distanceFromCenter;
                            }
                            len = Math.PI * radius * Math.abs(pieces[i].angle) / 180.0;
                        }
                        switchPieces[k].lanes[j].lengthPath += len;
                    }
                }
            }

            for (var j = 0; j < lanes.length; j++) {
                for (var i = switchPieces[switchPieces.length - 1].index + 1; i < pieces.length; i++) {
                    var len = 0.0;
                    if (pieces[i].length !== undefined) {
                        len = pieces[i].length;
                    } else {
                        var radius = pieces[i].radius;
                        if (pieces[i].angle > 0) {
                            radius -= lanes[j].distanceFromCenter;
                        } else {
                            radius += lanes[j].distanceFromCenter;
                        }
                        len = Math.PI * radius * Math.abs(pieces[i].angle) / 180.0;
                    }
                    switchPieces[switchPieces.length - 1].lanes[j].lengthPath += len;
                }
                for (var i = 0; i < switchPieces[0].index; i++) {
                    var len = 0.0;
                    if (pieces[i].length !== undefined) {
                        len = pieces[i].length;
                    } else {
                        var radius = pieces[i].radius;
                        if (pieces[i].angle > 0) {
                            radius -= lanes[j].distanceFromCenter;
                        } else {
                            radius += lanes[j].distanceFromCenter;
                        }
                        len = Math.PI * radius * Math.abs(pieces[i].angle) / 180.0;
                    }
                    switchPieces[switchPieces.length - 1].lanes[j].lengthPath += len;
                }
            }

            for (var i = 0; i < switchPieces.length; i++) {
                for (var j = 0; j < lanes.length; j++) {
                    console.log(switchPieces[i].index + ' index: ' + switchPieces[i].lanes[j].index + ' length: ' + switchPieces[i].lanes[j].lengthPath);
                }
            }

            var i = 0;
            while (i < pieces.length) {
                if (pieces[i].length === undefined) {
                    i++;
                } else {
                    var p = new path();
                    p.startIdx = i;
                    var len = pieces[i].length;
                    i++;
                    while (i < pieces.length && pieces[i].length !== undefined) {
                        len += pieces[i].length;
                        i++;
                    }
                    p.lengthPath = len;
                    paths.push(p);
                }
            }
            if (pieces[pieces.length - 1].length !== undefined) {
                paths[paths.length - 1].lengthPath += paths[0].lengthPath;
            }

            var longestLen = 0.0;
            for (var i = 0; i < paths.length; i++) {
                if (paths[i].lengthPath > longestLen) {
                    longestLen = paths[i].lengthPath;
                }
            }

            for (var i = 0; i < paths.length; i++) {
                if (paths[i].lengthPath === longestLen) {
                    longestPathIndex = i;
                }
            }
            console.log('length = ' + myCar.dimensions.length + ' --- width = ' + myCar.dimensions.width);
        } else if (data.msgType === 'gameStart') {
            send({
                msgType: "throttle",
                data: throttle
            });
            console.log('Race started');
        } else if (data.msgType === 'gameEnd') {
            console.log('Race ended');
        } else if (data.msgType === 'turboAvailable') {
            console.log(data);
            turboOn = true;
        } else if (data.msgType === 'turboStart') {
            console.log(data);
            if (data.data.name === myCarId.name && data.data.color === myCarId.color) {
                turboOn = false;
            }
        } else if (data.msgType === 'lapFinished') {
            console.log(data);
            if (data.data.car.name === myCarId.name && data.data.car.color === myCarId.color) {
if (qualifying) {
                if (numCrash === 0) {
                    twoLap++;
                    if (twoLap === 2) {
                        FslipNoCrash = FslipCoef;
                        FslipCoef = 0.5 * (FslipNoCrash + FslipCrash);
                        twoLap = 0;
                    }
                } else {
                    twoLap = 0;
                }
                numCrash = 0;
}
                console.log('FslipCoef = ' + FslipCoef + ' --- FslipCrash = ' + FslipCrash + ' --- FslipNoCrash = ' + FslipNoCrash);
            }
        } else if (data.msgType === 'crash') {
            console.log(data);
            if (data.data.name === myCarId.name && data.data.color === myCarId.color) {
if (qualifying) {
                FslipCrash = FslipCoef;
                FslipCoef = 0.5 * (FslipNoCrash + FslipCrash);
                numCrash++;
} else {
                FslipCoef -= 0.01;
}
                console.log('FslipCoef = ' + FslipCoef + ' --- FslipCrash = ' + FslipCrash + ' --- FslipNoCrash = ' + FslipNoCrash);
            }
        } else if (data.msgType === 'spawn') {
            console.log(data);
            if (data.data.name === myCarId.name && data.data.color === myCarId.color) {
            }
        } else {
            console.log(data);
        }
	
	    send({
            msgType: "throttle",
            data: throttle
        });
    }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
